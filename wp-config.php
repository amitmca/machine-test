<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'wordpress' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'FU<$zX$y%J`&98D](wSl:g7uZA*pYS|t6@c~Wyx.%|XCOy^b,oWwMlccOGk86g~B' );
define( 'SECURE_AUTH_KEY',  'kB;hf2lN>IWU=3:H]#DW=mQ8]I8L[8(+ZX8!(CH<H1{oR++If yQ3ZZAL%o,[yQn' );
define( 'LOGGED_IN_KEY',    'm:@?FjfbV))D+oUDQPt2X.M0`W=;dp :=]$?HV7u#3D6legx#Ofy9#6#~d7uS;`q' );
define( 'NONCE_KEY',        '$;|8 FUn%3w^2}qpTb^p9}nmbP]dS3Hqg#R=K8zf%j4u+{Y_%;LT^]BvCgGB5UmX' );
define( 'AUTH_SALT',        '*p;=9V03rv2|XvcU>/CVx-c!5A(5}Fupu}mz&fUwtPL,]q0{ e]zEN%C*]01Zdp/' );
define( 'SECURE_AUTH_SALT', '#8za*v[pUd6do7!?*knUba^pL> N=b0QcOMb5uRs8ftOTvw!|-g6L||TvN@-_LSd' );
define( 'LOGGED_IN_SALT',   '^8QLn^iEa;ANa+W[LKP-Zdi6l>ln+*/++u,2M{A ej[ztk(xq!8mfG$FJu!I+Wod' );
define( 'NONCE_SALT',       'hiDMg1l^O^cO}`Gsl(`5%0p>Y(/COdSjKl:]0NQJr?l<`Q!gRg(=< ?M65DI6Xd`' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
